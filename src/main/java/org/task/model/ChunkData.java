package org.task.model;

import java.util.List;

public class ChunkData {

    private final int lines;
    private final int chars;
    private final List<String> data;

    public ChunkData(int lines, int chars, List<String> data) {
        this.lines = lines;
        this.chars = chars;
        this.data = data;
    }

    public int getLines() {
        return lines;
    }

    public int getChars() {
        return chars;
    }

    public List<String> getData() {
        return data;
    }
}
