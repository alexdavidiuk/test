package org.task.model;

import java.util.Objects;

public class Match {

    private final String word;
    private final int linesOffset;
    private final int charsOffset;

    public Match(String word, int linesOffset, int charsOffset) {
        this.word = word;
        this.linesOffset = linesOffset;
        this.charsOffset = charsOffset;
    }

    public String getWord() {
        return word;
    }

    public int getLinesOffset() {
        return linesOffset;
    }

    public int getCharsOffset() {
        return charsOffset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return Objects.equals(word, match.word);
    }

    @Override
    public int hashCode() {
        return Objects.hash(word);
    }

    @Override
    public String toString() {
        return "{" +
                "linesOffset=" + linesOffset +
                ", charsOffset=" + charsOffset +
                '}';
    }
}
