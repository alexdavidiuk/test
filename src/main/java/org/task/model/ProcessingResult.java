package org.task.model;

import java.util.ArrayList;
import java.util.List;

public class ProcessingResult {

    private final List<Match> matches = new ArrayList<>();

    public ProcessingResult(List<Match> matches) {
        this.matches.addAll(matches);
    }

    public List<Match> getMatches() {
        return matches;
    }
}
