package org.task.service;

import org.task.model.ChunkData;
import org.task.model.Match;
import org.task.model.ProcessingResult;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.TimeUnit;

public class Orchestrator {

    private final String path;
    private final int batchSize;
    private final int parallelismLevel;

    private final List<Matcher> workers;
    private final BlockingQueue<ChunkData> processingQueue;
    private final ConcurrentLinkedQueue<ProcessingResult> resultQueue;

    public Orchestrator(String path, int batchSize, int parallelismLevel, int queueCapacity) {
        this.path = path;
        this.batchSize = batchSize;
        this.parallelismLevel = parallelismLevel;

        workers = new ArrayList<>(parallelismLevel);
        processingQueue = new LinkedBlockingDeque<>(queueCapacity);
        resultQueue = new ConcurrentLinkedQueue<>();
    }

    public void processFile() throws InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(parallelismLevel);
        setUpWorkers(executor);

        Reader reader = new Reader(batchSize, processingQueue);
        reader.consumeFile(path);

        //TODO: not the best way to wait for result, redo with message
        while (!processingQueue.isEmpty()) {
            TimeUnit.MILLISECONDS.sleep(500);
        }

        Aggregator aggregator = new Aggregator(resultQueue);
        for (Map.Entry<Match, List<Match>> entry : aggregator.consumeResults().entrySet()) {
            System.out.print(entry.getKey().getWord() + "->");
            System.out.println(entry.getValue());
        }

        tearDownWorkers(executor);
    }

    private void setUpWorkers(ExecutorService executor) {
        for (int i = 0; i < parallelismLevel; ++i) {
            Matcher m = new Matcher(processingQueue, resultQueue);
            workers.add(m);
            executor.submit(m);
        }
    }

    private void tearDownWorkers(ExecutorService executor) {
        executor.shutdown();
        workers.forEach(Matcher::shutdown);
    }
}
