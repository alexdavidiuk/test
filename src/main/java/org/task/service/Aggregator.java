package org.task.service;

import org.task.model.Match;
import org.task.model.ProcessingResult;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Aggregator {

    private final ConcurrentLinkedQueue<ProcessingResult> resultQueue;

    public Aggregator(ConcurrentLinkedQueue<ProcessingResult> resultQueue) {
        this.resultQueue = resultQueue;
    }

    public Map<Match, List<Match>> consumeResults() {
        return resultQueue.parallelStream()
                .flatMap(pr -> pr.getMatches().parallelStream())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.toList()));
    }
}
