package org.task.service;

import org.task.model.ChunkData;
import org.task.model.Match;
import org.task.model.ProcessingResult;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

public class Matcher implements Runnable {

    private static final List<String> words = Arrays.asList("James", "John", "Robert", "Michael", "William", "David",
            "Richard", "Charles", "Joseph", "Thomas", "Christopher", "Daniel", "Paul", "Mark", "Donald", "George",
            "Kenneth", "Steven", "Edward", "Brian", "Ronald", "Anthony", "Kevin", "Jason", "Matthew", "Gary",
            "Timothy", "Jose", "Larry", "Jeffrey", "Frank", "Scott", "Eric", "Stephen", "Andrew", "Raymond",
            "Gregory", "Joshua", "Jerry", "Dennis", "Walter", "Patrick", "Peter", "Harold", "Douglas",
            "Henry", "Carl", "Arthur", "Ryan", "Roger");

    private final AtomicBoolean running;
    private final BlockingQueue<ChunkData> processingQueue;
    private final ConcurrentLinkedQueue<ProcessingResult> resultQueue;

    public Matcher(BlockingQueue<ChunkData> processingQueue, ConcurrentLinkedQueue<ProcessingResult> resultQueue) {
        this.running = new AtomicBoolean(true);
        this.processingQueue = processingQueue;
        this.resultQueue = resultQueue;
    }

    public void shutdown() {
        running.compareAndSet(true, false);
    }

    @Override
    public void run() {
        while (running.get()) {
            try {
                ChunkData chunk = processingQueue.poll(1, TimeUnit.SECONDS);
                processChunk(chunk).ifPresent(resultQueue::add);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Optional<ProcessingResult> processChunk(ChunkData chunk) {
        Optional<ProcessingResult> result = Optional.empty();
        if (chunk != null) {
            List<Match> matches = words.parallelStream()
                    .map(w -> checkWord(w, chunk))
                    .filter(l -> !l.isEmpty())
                    .flatMap(List::stream)
                    .collect(Collectors.toList());
            if (!matches.isEmpty()) {
                result = Optional.of(new ProcessingResult(matches));
            }
        }
        return result;
    }

    private List<Match> checkWord(String word, ChunkData chunk) {
        List<Match> matches = new ArrayList<>();

        int chunkOffset = 0;
        int length = word.length();
        for (int i = 0; i < chunk.getData().size(); ++i) {
            String line = chunk.getData().get(i);
            int p = line.indexOf(word);
            while (p >= 0) {
                matches.add(new Match(word, chunk.getLines() + i, chunkOffset + chunk.getChars() + p));
                p = line.indexOf(word, p + length);
            }
            chunkOffset += line.length();
        }

        return matches;
    }
}
