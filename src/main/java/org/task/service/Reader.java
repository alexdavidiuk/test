package org.task.service;

import org.task.model.ChunkData;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Reader {

    private final int batchSize;
    private final BlockingQueue<ChunkData> processingQueue;

    public Reader(int batchSize, BlockingQueue<ChunkData> processingQueue) {
        this.batchSize = batchSize;
        this.processingQueue = processingQueue;
    }

    // TODO: kill me or refactor
    public void consumeFile(String path) {
        int linesOffset = 0, nextLinesOffset = 0;
        int charsOffset = 0, nextCharsOffset = 0;
        List<String> data = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(path));

            String line = reader.readLine();
            while (line != null) {
                data.add(line);
                ++nextLinesOffset;
                nextCharsOffset += line.length();

                if (nextLinesOffset % batchSize == 0) {
                    processingQueue.put(new ChunkData(linesOffset, charsOffset, data));

                    data = new ArrayList<>();
                    linesOffset = nextLinesOffset;
                    charsOffset = nextCharsOffset;
                }

                line = reader.readLine();
            }

            if (data.size() != 0) {
                processingQueue.put(new ChunkData(linesOffset, charsOffset, data));
            }

            reader.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
