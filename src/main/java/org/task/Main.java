package org.task;

import org.task.service.Orchestrator;

import static java.lang.Integer.parseInt;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        String filePath = args[0];
        int batchSize = parseInt(args[1]);
        int parallelismLevel = parseInt(args[2]);

        Orchestrator orchestrator = new Orchestrator(filePath, batchSize, parallelismLevel, parallelismLevel + 1);
        orchestrator.processFile();
    }
}
