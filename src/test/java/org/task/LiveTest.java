package org.task;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

// TODO: No time for proper Unit-tests, quick way to check where specific word is located in text
public class LiveTest {
    public static void main(String[] args) {
        try {
            String data = Files.lines(Paths.get(args[0])).collect(Collectors.joining());
            System.out.println(data.indexOf("Timothy"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
