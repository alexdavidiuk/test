Main.java is an entry point for task computation, usage example:

    java -jar target/task.jar path/to/data batch cores
    
Where:
* path/to/data - path to file with data
* batch - how many lines include in single batch
* cores - number of threads to use when splitting work for Matchers

Simple command would look like: `java -jar target/task.jar src/test/resources/big.txt 1000 4`

Simple way to check how time and parallelism level impacts performance:

    time java -jar target/task.jar src/test/resources/100x_big.txt 1000 4
    real    0m3.892s
    user    0m19.255s
    sys     0m0.774s

    >>>
    
    time java -jar target/task.jar src/test/resources/100x_big.txt 1000 1 (+ remove parallelStream usage)
    real    0m12.683s
    user    0m14.837s
    sys     0m0.683s